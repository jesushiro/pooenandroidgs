package com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel;

import java.util.ArrayList;

public interface FindCallback<DataObject> {
    public void done(ArrayList<DataObject> objects, DataException e);
}
