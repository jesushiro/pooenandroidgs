package com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.pra1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.trabajoautonomo.santanagilbert.facci.pooenandroid.R;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.DataException;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.DataObject;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.DataQuery;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.FindCallback;

import java.util.ArrayList;

public class ResultsActivity extends AppCompatActivity implements  ListView.OnItemClickListener {

    private View mProgressView;
    private ListView mListView;
    public ResultListAdapter m_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        String user_email = getIntent().getStringExtra("user_email");

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("LISTA DE PRODUCTO");

        mListView = (ListView) findViewById(R.id.listView);
        mProgressView = findViewById(R.id.progress);


        mListView.setOnItemClickListener(this);

        showProgress(true);

        // ************************************************************************
        // INICIO - CODE3
        //
        DataQuery query = DataQuery.get("item");
        query.findInBackground("", "", DataQuery.OPERATOR_ALL, new FindCallback<DataObject>() {
            @Override
            public void done(ArrayList<DataObject> dataObjects, DataException e) {
                if (e == null) {
                    if (dataObjects.size() != 0) {
                        m_adapter = new ResultListAdapter(ResultsActivity.this, null);

                        m_adapter.m_array = dataObjects;
                        m_adapter.mActivity = ResultsActivity.this;

                        showProgress(false);
                        mListView.setAdapter(m_adapter);
                    }
                } else {
                    // Error

                }
            }
        });
        // FIN - CODE3
        // ************************************************************************


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        DataObject object = (DataObject) m_adapter.m_array.get(position);
        Intent intent;
        intent = new Intent(ResultsActivity.this, DetailActivity.class);
        intent.putExtra("object_id",  object.m_objectId);
        startActivity(intent);


        // FIN - CODE5


    }

    private void showProgress(final boolean show) {

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mListView.setVisibility(show ? View.GONE : View.VISIBLE);

    }



}