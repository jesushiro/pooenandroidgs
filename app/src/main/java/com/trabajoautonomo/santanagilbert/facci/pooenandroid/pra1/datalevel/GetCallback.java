package com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel;

public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
