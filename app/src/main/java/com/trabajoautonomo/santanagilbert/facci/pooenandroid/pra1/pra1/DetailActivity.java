package com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.trabajoautonomo.santanagilbert.facci.pooenandroid.R;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.DataException;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.DataObject;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.DataQuery;
import com.trabajoautonomo.santanagilbert.facci.pooenandroid.pra1.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("DETALLE DEL PRODUCTO");
        String object_id = getIntent().getStringExtra("object_id");
        TextView description = (TextView) findViewById(R.id.Description);
        description.setMovementMethod(LinkMovementMethod.getInstance());

        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {

            @Override
            public void done(DataObject object, DataException e) {
                if (e ==null){
                    TextView title = (TextView) findViewById(R.id.Name);
                    TextView price = (TextView) findViewById(R.id.Price);
                    TextView description = (TextView) findViewById(R.id.Description);
                    ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);
                    title.setText((String) object.get("name"));
                    price.setText((String) object.get("price") + "\u0024");
                    description.setText((String) object.get("description"));
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                }else{
                    //Error
                }
            }
        });

        // FIN - CODE6

    }
}
